from turtle import Turtle


class Paddle(Turtle):
    def __init__(self):
        super().__init__()
        self.shape("square")
        self.color("white")
        self.pu()
        self.speed("fastest")
        self.turtlesize(stretch_wid=4.5, stretch_len=1)

    def create_paddle(self, sx, sy):
        self.setx(sx)
        self.sety(sy)

    def up(self):
        self.goto(self.xcor(), self.ycor() + 20)

    def down(self):
        self.goto(self.xcor(), self.ycor() - 20)
