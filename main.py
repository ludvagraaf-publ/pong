""" The retro game from ATARI - PONG
    ================================
    Each ball touch with paddle increases speed of ball. Infinite score.

    Create just for learning purpose
 """

from turtle import Screen
from paddle import Paddle
from ball import Ball
from score import Scoreboard
import time

screen = Screen()
screen.bgcolor("black")
screen.title("PONG game")
screen.setup(width=800, height=600)

screen.tracer(0)
screen.listen()

r_paddle = Paddle()
r_paddle.create_paddle(350, 0)
l_paddle = Paddle()
l_paddle.create_paddle(-350, 0)
ball = Ball()
score = Scoreboard()

# keyboard controlling game
screen.onkey(r_paddle.up, "Up")
screen.onkey(r_paddle.down, "Down")
screen.onkey(l_paddle.up, "s")
screen.onkey(l_paddle.down, "x")
screen.onkeypress(screen.bye, "q")

game_on = True
while game_on:
    screen.update()
    time.sleep(ball.move_speed)
    ball.ball_moving()
    if ball.ycor() > 280 or ball.ycor() < -280:
        ball.bounce()
    # collision with paddle, return the ball
    if ball.xcor() < -320 or ball.xcor() > 320:
        if ball.distance(r_paddle) < 51 or ball.distance(l_paddle) < 51:
            ball.go_back()
    # ball out of bounds, thus score, reset opposite dir
    if ball.xcor() < -340:
        ball.reset_pos()
        score.r_point()

    if ball.xcor() > 340:
        ball.reset_pos()
        score.l_point()

    # let's keep paddles within playground boundaries
    if r_paddle.ycor() > 250:
        r_paddle.goto(350, 250)
    elif r_paddle.ycor() < -240:
        r_paddle.goto(350, -240)
    if l_paddle.ycor() > 250:
        l_paddle.goto(-350, 250)
    elif l_paddle.ycor() < -240:
        l_paddle.goto(-350, -240)

screen.exitonclick()
