# pong

PONG is a retro game for two players created in ATARI company. This
is it's representation rewritten in Python. It uses [Tk interface package](https://docs.python.org/3/library/tkinter.html#module-tkinter)


## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install tkinter.

```bash
pip install tkinter
```

On older MacOS (BigSur) you will need to install it from Brew.
Install [brew](https://brew.sh/) first if you haven't done so already.
Then install tkinter for specific version of Python (eg 3.10):

```bash
brew install python-tk@3.10
```

## Usage

In the directory where you download/clone:

```bash
cd ~/Download/pong/
python ./main.py
```

### Playing the game:

The two paddles return the ball back and forth.
The score is displayed at the top of the screen.
Left paddle controls: "s" and "x" keys. 
The right uses is "Up" and "Down" arrows. For quit press "q".

Enjoy the game!

